using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{
	public static Amiibo[] LoadXMLData(string filename)
	{
		List<Amiibo> amiibos = new List<Amiibo>();

		TextAsset textData = Resources.Load<TextAsset>("Amiibos");

		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(textData.text));

		string xmlPathPattern = "//amiibos/amiibo";
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);

		foreach (XmlNode node in myNodeList)
		{
			Amiibo amiibo = new Amiibo(int.Parse(node.Attributes["identifier"].Value), node.Attributes["image"].Value, node.InnerText);
			amiibos.Add(amiibo);
		}

		return amiibos.ToArray();
	}
}
