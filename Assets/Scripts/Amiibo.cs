﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amiibo {

	private int identifier;
	private string fullName;
	private Sprite amiiboSprite;

	public int Identifier { get => identifier ; }
	public string FullName { get => fullName; }
	public Sprite AmiiboSprite { get => amiiboSprite; }

	public Amiibo(int identifier,  string spriteName, string fullName) {
		this.identifier = identifier;
		this.fullName = fullName;
		this.amiiboSprite = Resources.Load<Sprite>("images/" + spriteName);
	}
	
}

